package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
	"log"
	"os"
	"io"
	"mime/multipart"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/gorilla/mux"
	"gopkg.in/gomail.v2"
)

// DB : the database connection
var DB *gorm.DB
var mailer *gomail.Dialer

// Heartbeat : the database model for the heartbeart table
type Heartbeat struct {
	gorm.Model
	Active	bool
	Triggered	bool
}

// Config : the database model for the alarm config
type Config struct {
	gorm.Model
	Active	bool
	Canceled	bool
}

// HeartbeatPing : request data from alarm
type HeartbeatPing struct {
	Type	string
	Triggered	bool
}

// HeartbeatPingResponse : response data to alarm
type HeartbeatPingResponse struct {
	Active	bool
	Canceled	bool
}

// ImageUploadResponse : response data to alarm
type ImageUploadResponse struct {
	Status	string
	Filename	string
	Size	int
}

// ConfigRequest : the config update
type ConfigRequest struct {
	Active	bool
	Canceled	bool
}

func init() {
	var err error

	if DB, err = gorm.Open("postgres", "host=localhost user=alarm dbname=alarm sslmode=disable password=##Kp>Ng6U8n}K?Ph"); err != nil {
		panic(fmt.Sprintf("No error should happen when connecting to test database, but got err=%+v", err))
	}
	DB.LogMode(true)
	DB.DB().SetMaxIdleConns(10)
	DB.Debug().AutoMigrate(&Heartbeat{}, &Config{})
	mailer = gomail.NewDialer("smtp.gmail.com", 587, "wmelon@gmail.com", "password")
}

func main() {
	router := mux.NewRouter()
	router.Path("/heartbeat").Methods("POST").HandlerFunc(heartbeat)
	router.Path("/control").Methods("POST").HandlerFunc(control)
	router.Path("/uploadImage").Methods("POST").HandlerFunc(postImage)
	fmt.Println("Starting up on port 8001")
	log.Fatal(http.ListenAndServe(":8001", router))
}

func heartbeat(w http.ResponseWriter, r *http.Request) {
	var config Config
	var heartBeat Heartbeat
	var t HeartbeatPing
	var result HeartbeatPingResponse
    r.ParseForm()
    for key := range r.Form {
        err := json.Unmarshal([]byte(key), &t)
        if err != nil {
            println(fmt.Sprintf("err=%+v", err))
        }
    }
	DB.First(&config)
    heartBeat.Active = config.Active
	heartBeat.Triggered = t.Triggered
	heartBeat.CreatedAt = time.Now()
	DB.Create(&heartBeat)
	
	if t.Triggered == true {
		m := gomail.NewMessage()
		m.SetHeader("From", "greg@virtuallite.com")
		m.SetHeader("To", "greg@virtuallite.com")
		m.SetHeader("Subject", "Alarm has been triggered")
		m.SetBody("text/plain", "The RV trailer's alarm has been triggered.")
		if err := mailer.DialAndSend(m); err != nil {
			panic(err)
		}
	}

	result.Active = config.Active
	result.Canceled = config.Canceled
	json, err := json.Marshal(result)
	w.Header().Set("Content-Type", "application/json")
	if err == nil {
		w.Write(json)
	} else {
		w.Write([]byte("{ 'error': 'could not write to database' }"))
	}
}

func control(w http.ResponseWriter, r *http.Request) {
	var config Config
	var t ConfigRequest
    r.ParseForm()
    for key := range r.Form {
        err := json.Unmarshal([]byte(key), &t)
        if err != nil {
            println(fmt.Sprintf("err=%+v", err))
        }
    }
	DB.First(&config)
    config.Active = t.Active
	config.Canceled = t.Canceled
	config.UpdatedAt = time.Now()
	DB.Save(&config)
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("{ 'status': 'OK' }"))
}

func postImage(w http.ResponseWriter, r *http.Request) {
	var (  
		status int  
		err  error  
	) 
	var response ImageUploadResponse
	defer func() {  
		if nil != err {  
			http.Error(w, err.Error(), status)  
		}  
	}()
	// parse request  
	// const _24K = (1 << 20) * 24  
	if err = r.ParseMultipartForm(32 << 20); nil != err {  
		status = http.StatusInternalServerError  
		return  
	} 
	for _, fheaders := range r.MultipartForm.File {  
		for _, hdr := range fheaders {  
			// open uploaded  
			var infile multipart.File  
			if infile, err = hdr.Open(); nil != err {  
					status = http.StatusInternalServerError  
					return  
			}  
			// open destination  
			var outfile *os.File  
			if outfile, err = os.Create("./images/" + hdr.Filename); nil != err {  
					status = http.StatusInternalServerError  
					return  
			}  
			// 32K buffer copy  
			var written int64  
			if written, err = io.Copy(outfile, infile); nil != err {  
					status = http.StatusInternalServerError  
					return  
			}
			response.Status = "OK"
			response.Filename = hdr.Filename
			response.Size = int(written)
			json, err := json.Marshal(response)
			if err != nil {
				return
			}
			w.Header().Set("Content-Type", "application/json")
			w.Write(json)  
		}  
	}
}